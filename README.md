# Ubiq Golang Community Samples

This repository contains sample projects for working with the Ubiq Security Golang libraries.

To learn more about working with the Ubiq Security libraries, go to [the official documentation](https://dev.ubiqsecurity.com/docs/what-is-ubiq).

## Listing

- *Ubiq-File-Lambda* - AWS Lambda/API Gateway system for encrypting and decrypting files stored in an S3 Bucket.
    - [README.md](ubiq-file-lambda/README.md)