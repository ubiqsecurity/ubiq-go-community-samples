package main

import (
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"gitlab.com/ubiqsecurity/ubiq-go"
	// "gitlab.com/ubiqsecurity/ubiq-go"
)

type UploadResponse struct {
	Guid     string
	Path     string
	Metadata map[string]string
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	res := events.APIGatewayProxyResponse{}

	cfg, err := config.LoadDefaultConfig(context.TODO())

	if err != nil {
		return res, err
	}

	client := s3.NewFromConfig(cfg)

	bucketName := getenv("BUCKET_NAME", "ubiq-file-share-poc")
	objectKey := request.PathParameters["id"]

	// Upload with metadata
	result, err := client.GetObject(context.TODO(), &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(objectKey),
	})

	if err != nil {
		res.Body = fmt.Sprintf("Couldn't get object %v:%v. Here's why: %v\n", bucketName, objectKey, err)
		res.StatusCode = 500
		return res, err
	}

	defer result.Body.Close()

	content, err := io.ReadAll(result.Body)

	if err != nil {
		res.Body = fmt.Sprintf("Couldn't read file: %v", err)
		res.StatusCode = 500
		return res, err
	}

	// Create Ubiq credentials object
	credentials, err := ubiq.NewCredentials()
	if err != nil {
		res.Body = fmt.Sprintf("credentials failure: %v", err)
		res.StatusCode = 500
		return res, err
	}

	// Decrypt
	decrypted_data, enc_err := ubiq.Decrypt(credentials, content)
	if enc_err != nil {
		res.Body = fmt.Sprintf("decryption failure: %v", enc_err)
		res.StatusCode = 500
		return res, err
	}

	if err != nil {
		return res, err
	}

	// Return content as a Base64 String (Lambda expects strings, IsBase64Encoded has it decode for us.)
	base64Text := make([]byte, base64.StdEncoding.EncodedLen(len(decrypted_data)))
	base64.StdEncoding.Encode(base64Text, decrypted_data)

	return events.APIGatewayProxyResponse{
		Body:       string(base64Text), // Must be a string
		StatusCode: 200,
		Headers: map[string]string{
			"Content-Type":                 http.DetectContentType(decrypted_data), // Needed to properly assign MIME type
			"Content-Disposition":          fmt.Sprintf("attachment; filename=%v", result.Metadata["filename"]),
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Methods": "POST, GET, OPTIONS",
			"Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
		},
		IsBase64Encoded: true, // Tells Lambda to Base64 Decode our content
	}, nil
}

func main() {
	lambda.Start(handler)
}
