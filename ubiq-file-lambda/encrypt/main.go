package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/google/uuid"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/grokify/go-awslambda"

	"gitlab.com/ubiqsecurity/ubiq-go"
)

type UploadResponse struct {
	Guid     string
	Path     string
	Metadata map[string]string
}

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	res := events.APIGatewayProxyResponse{}

	// Create a reader for the request
	requestReader, err := awslambda.NewReaderMultipart(request)
	if err != nil {
		res.Body = fmt.Sprintf("Failed to create reader: %v", err)
		res.StatusCode = 500
		return res, err
	}

	// Get the MIME part of the request
	part, err := requestReader.NextPart()
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Body:       fmt.Sprintf("Something went wrong: %v", err),
		}, err
	}

	// Read the content
	content, err := io.ReadAll(part)

	if err != nil {
		res.Body = fmt.Sprintf("Failed to read file content: %v", err)
		res.StatusCode = 500
		return res, err
	}

	cfg, err := config.LoadDefaultConfig(context.TODO())

	if err != nil {
		res.Body = fmt.Sprintf("Failed to load configuration: %v", err)
		res.StatusCode = 500
		return res, err
	}

	client := s3.NewFromConfig(cfg)

	metadata := map[string]string{
		"Filename":      part.FileName(),
		"FileExtension": filepath.Ext(part.FileName()),
		"UploadedBy":    request.RequestContext.Identity.SourceIP,
	}

	// Create Ubiq credentials object
	credentials, err := ubiq.NewCredentials()
	if err != nil {
		res.Body = fmt.Sprintf("credentials failure: %v", err)
		res.StatusCode = 500
		return res, err
	}

	// Encrypt
	encrypted_data, enc_err := ubiq.Encrypt(credentials, content)
	if enc_err != nil {
		res.Body = fmt.Sprintf("encryption failure: %v", enc_err)
		res.StatusCode = 500
		return res, err
	}
	// Load into a Reader for S3 Put
	encryptedDataReader := bytes.NewReader(encrypted_data)

	bucketName := getenv("BUCKET_NAME", "ubiq-file-share-poc")
	objectKey := uuid.New().String()

	// Upload with metadata
	_, err = client.PutObject(context.TODO(), &s3.PutObjectInput{
		Bucket:   aws.String(bucketName),
		Key:      aws.String(objectKey),
		Body:     encryptedDataReader,
		Metadata: metadata,
	})

	if err != nil {
		res.Body = fmt.Sprintf("Couldn't upload file to %v:%v. Here's why: %v\n",
			bucketName, objectKey, err)
		res.StatusCode = 500
		return res, err
	}

	// Create Return Payload
	upload_bytes, err := json.Marshal(UploadResponse{
		Guid:     objectKey,
		Path:     fmt.Sprintf("s3://%v/%v", bucketName, objectKey),
		Metadata: metadata,
	})

	if err != nil {
		res.Body = "Failed to upload"
		res.StatusCode = 500
		return res, err
	}

	return events.APIGatewayProxyResponse{
		Body:       string(upload_bytes),
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(handler)
}
