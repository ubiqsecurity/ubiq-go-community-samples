require (
	github.com/aws/aws-lambda-go v1.47.0
	github.com/aws/aws-sdk-go v1.53.17 // indirect
	github.com/aws/aws-sdk-go-v2 v1.27.1 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.27.17 // indirect
	github.com/aws/aws-sdk-go-v2/service/s3 v1.55.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/grokify/go-awslambda v0.2.1 // indirect
	gitlab.com/ubiqsecurity/ubiq-go v0.0.3 // indirect
)

replace gopkg.in/yaml.v2 => gopkg.in/yaml.v2 v2.2.8

module encrypt

go 1.16
