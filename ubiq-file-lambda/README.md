# Ubiq AWS File Sharing Lambda

This file sharing utility allows you to deploy code to AWS Lambda and trigger it via API Gateway.

```bash
.
├── Makefile                    <-- Make to automate build
├── README.md                   <-- This instructions file
├── encrypt                     <-- Source code for encrypt lambda function
│   └── main.go                 <-- Lambda function code
├── decrypt                     <-- Source code for encrypt lambda function
│   └── main.go                 <-- Lambda function code
└── template.yaml
```

The AWS SAM (Serverless Application Model) utility is used to automate creation and deployment. The Ubiq Library will encrypt 

## Requirements

* AWS CLI already configured with Administrator permission
* [Docker installed](https://www.docker.com/community-edition)
* [Golang](https://golang.org)
* SAM CLI - [Install the SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)

## Setup process

### Installing dependencies & building the target 

In this example we use the built-in `sam build` to automatically download all the dependencies and package our build target.   
Read more about [SAM Build here](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-build.html) 

The `sam build` command is wrapped inside of the `Makefile`. To execute this simply run
 
```shell
make
```

### Local development

**Invoking function locally through local API Gateway**

```bash
sam local start-api
```

If the previous command ran successfully you should now be able to hit the following local endpoints to invoke your function:
- **POST** `http://localhost:3000/encrypt` -  file included as form-data
- **GET** `http://localhost:3000/decrypt/{id}` -  where ID is the GUID returned from encrypting.

**SAM CLI** is used to emulate both Lambda and API Gateway locally and uses our `template.yaml` to understand how to bootstrap this environment (runtime, where the source code is, etc.).

## Packaging and deployment

### Setup 
AWS Lambda Golang runtime requires a flat folder with the executable generated on build step. SAM will use `CodeUri` property to know where to look up for the application:

```yaml
...
    EncryptFileFunction:
        Type: AWS::Serverless::Function
        Properties:
            CodeUri: encrypt/
            ...
```

#### Permissions 

API Keys are required to encrypt and decrypt with the Ubiq library. Ensure either:
- Keys are included in the appropriate section(s) (See `Environment:`) of the `template.yaml`
- OR Environment variables are set up in AWS Lambda after deploying. https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html

The API Key should have an association with an Unstructured Dataset.

The user deploying will need the following IAM permissions to deploy:
```
iam:CreateRole
iam:DeleteRole
iam:DetachRolePolicy
iam:AttachRolePolicy
iam:GetRole
iam:PassRole
cloudformation:DescribeStacks
cloudformation:GetTemplateSummary
cloudformation:CreateChangeset
cloudformation:DeleteStack
lambda:CreateFunction
lambda:DeleteFunction
lambda:TagResource
lambda:GetFunction
lambda:UpdateFunction
```

The roles for the lambda function will automatically be created with the following permissions:
```
s3:PutObject
s3:GetObject
```

#### Bucket Name

The template includes configuration for creating an S3 bucket for use with the provided lambdas. The `BucketName` will need to be updated prior to use. If you wish to use an existing bucket, remove the `FileShareBucket` section under `Resources`.

The `BucketName` is also used as an Environment variable and part of the S3 Read/Write Permissions Policies added for each lambda. These will need to match either the name of your intended bucket or the bucket craeted in `FileShareBucket`.

### Deploying 
>**Note:** Remember to build the application first with `sam build`

To deploy your application for the first time, run the following in your shell:

```bash
sam deploy --guided
```


The command will package and deploy your application to AWS, with a series of prompts:

* **Stack Name**: The name of the stack to deploy to CloudFormation. This should be unique to your account and region, and a good starting point would be something matching your project name.
* **AWS Region**: The AWS region you want to deploy your app to.
* **Confirm changes before deploy**: If set to yes, any change sets will be shown to you before execution for manual review. If set to no, the AWS SAM CLI will automatically deploy application changes.
* **Allow SAM CLI IAM role creation**: Many AWS SAM templates, including this example, create AWS IAM roles required for the AWS Lambda function(s) included to access AWS services. By default, these are scoped down to minimum required permissions. To deploy an AWS CloudFormation stack which creates or modifies IAM roles, the `CAPABILITY_IAM` value for `capabilities` must be provided. If permission isn't provided through this prompt, to deploy this example you must explicitly pass `--capabilities CAPABILITY_IAM` to the `sam deploy` command.
* **Save arguments to samconfig.toml**: If set to yes, your choices will be saved to a configuration file inside the project, so that in the future you can just re-run `sam deploy` without parameters to deploy changes to your application.

You can find your API Gateway Endpoint URL in the output values displayed after deployment.

## Authentication

This sample is provided with the endpoints not requiring authentication. End users will still need to know the GUID associated with a file to retrieve it, but care should still be taken to prevent unauthorized users from encrypting files to your bucket and from retrieving files without permission. 

# Appendix

### Golang installation

Please ensure Go 1.x (where 'x' is the latest version) is installed as per the instructions on the official golang website: https://golang.org/doc/install

A quickstart way would be to use Homebrew, chocolatey or your linux package manager.

#### Homebrew (Mac)

Issue the following command from the terminal:

```shell
brew install golang
```

If it's already installed, run the following command to ensure it's the latest version:

```shell
brew update
brew upgrade golang
```

#### Chocolatey (Windows)

Issue the following command from the powershell:

```shell
choco install golang
```

If it's already installed, run the following command to ensure it's the latest version:

```shell
choco upgrade golang
```
